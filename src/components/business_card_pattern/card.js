import React from 'react';
import cardtext from './card_about_me.json';

const card = (props) => {

   let carousel_aboutme_header;
   let carousel_aboutme_paragraph;
   let carousel_aboutcompany_header;
   let carousel_aboutcompany_text;

   if (props.lang === 'en') {
      carousel_aboutme_header = cardtext.about.header_aboutme_en;
      carousel_aboutme_paragraph = cardtext.about.text_aboutme_en;
      carousel_aboutcompany_header = cardtext.about.header_aboutcompany_en;
      carousel_aboutcompany_text = cardtext.about.text_aboutcompany_en;
   } else if (props.lang === 'pl') {
      carousel_aboutme_header = cardtext.about.header_aboutme_pl;
      carousel_aboutme_paragraph = cardtext.about.text_aboutme_pl;
      carousel_aboutcompany_header = cardtext.about.header_aboutcompany_pl;
      carousel_aboutcompany_text = cardtext.about.text_aboutcompany_pl;
   }

   let id_carousel;
   let id_hash_carousel;
   let aboutme_carousel_content;
   let opinions_carousel_content;

   if (props.id === "aboutme_carousel_indicators") {
      id_carousel = "aboutme_carousel_indicators";
      id_hash_carousel = "#aboutme_carousel_indicators";

      opinions_carousel_content = <div>
         <ol className="carousel-indicators">
            <li data-target={id_hash_carousel} data-slide-to="0" className="active"></li>
            <li data-target={id_hash_carousel} data-slide-to="1"/>
         </ol>
         <div className="carousel-inner">
            <div className="carousel-item active">
               <section>
                  <h3 id="to_company">{carousel_aboutcompany_header}</h3>
                  <p>{carousel_aboutcompany_text}</p>
               </section>
            </div>
            <div className="carousel-item">
               <section>
                  <h3>{carousel_aboutme_header}</h3>
                  <p>{carousel_aboutme_paragraph}</p>
               </section>
            </div>
         </div>
      </div>

   } else if (props.id === "opinions_carousel_indicators") {
      id_carousel = "opinions_carousel_indicators";
      id_hash_carousel = "#opinions_carousel_indicators";

      opinions_carousel_content = <div>
         <ol className="carousel-indicators">
            <li data-target={id_hash_carousel} data-slide-to="0" className="active"></li>
            <li data-target={id_hash_carousel} data-slide-to="1"/>
            <li data-target={id_hash_carousel} data-slide-to="2"/>
         </ol>
         <div className="carousel-inner">
            <div className="carousel-item active">
               <section>
                  <p>{cardtext.lorem}</p>
               </section>
            </div>
            <div className="carousel-item">
               <section>
                  <p>{cardtext.lorem}</p>
               </section>
            </div>
            <div className="carousel-item">
               <section>
                  <p>{cardtext.lorem}</p>
               </section>
            </div>
         </div>
      </div>
   }

   return (<div id={id_carousel} className="carousel slide" data-ride="carousel" data-interval="false">

      <div>{opinions_carousel_content}</div>
      <a className="carousel-control-prev" href={id_hash_carousel} role="button" data-slide="prev">
         <span className="carousel-control-prev-icon" aria-hidden="true"></span>
         <span className="sr-only">Previous</span>
      </a>
      <a className="carousel-control-next" href={id_hash_carousel} role="button" data-slide="next">
         <span className="carousel-control-next-icon" aria-hidden="true"></span>
         <span className="sr-only">Next</span>
      </a>
   </div>);

};

export default card;
