import React, {Component} from 'react';

const imprint = () => {

   return (<div className="container-fluid">
      <h4>Attribution</h4>
      <div>Icons made by
         <a href="https://www.flaticon.com/authors/zlatko-najdenovski" title="Zlatko Najdenovski"> Zlatko Najdenovski </a>
         from
         <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com </a>
          is licensed by
         <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank"> CC 3.0 BY </a>
      </div>
      <div>Icons made by
         <a href="http://www.freepik.com" title="Freepik"> Freepik </a>
         from
         <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com </a>
         is licensed by
         <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank"> CC 3.0 BY </a>
      </div>
      <div>Icons made by
         <a href="https://www.flaticon.com/authors/good-ware" title="Good Ware"> Good Ware </a>
         from
         <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com </a>
         is licensed by
         <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank"> CC 3.0 BY </a>
      </div>
      <div>Icons made by
         <a href="https://www.flaticon.com/authors/zurb" title="Zurb"> Zurb </a>
         from
         <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com </a>
         is licensed by
         <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank"> CC 3.0 BY </a>
      </div>
   </div>);
};

export default imprint;
