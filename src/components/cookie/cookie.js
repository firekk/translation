import React, {Component} from 'react';
import cookie_data from './cookie_data.json';
import cookie_image from '../../assets/images/cookies/cookie.svg';

class Cookie extends Component {

   render() {
      return (<div className="alert fixed-bottom alert-dismissible show fade" role="alert">
         <div className="cookie_content">
            <img src={cookie_image} id="cookie_image_id"/>
            <strong>{cookie_data.cookie_header}</strong>
            <br></br>{cookie_data.about_cookie}
            <a href="#" className="alert-link">{cookie_data.readmore_cookie_link}</a>
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
      </div>)
   }
}

export default Cookie;
