import React, {Component} from 'react';
import './personal.css';
import phone from '../../assets/images/personal_contact_data/ic_phone.svg';
import mail from '../../assets/images/personal_contact_data/ic_mail.svg';
import linkedin from '../../assets/images/personal_contact_data/ic_linkedin.svg';
import book from '../../assets/images/company_values/ic_book.svg';
import conferences from '../../assets/images/company_values/ic_conferences.svg';
import business from '../../assets/images/company_values/ic_corporation.svg';
import technical_translations from '../../assets/images/company_values/ic_technical_translations.svg';
import personalvar from './personal_variables.json';

const personal = (props) => {

   let label;
   let icon;

   if (props.icon === personalvar.rendering_data.phone_number) {
      icon = phone;
      label = <a href={personalvar.telto.tel_format
}>
         {personalvar.label.phone_label}
         < /a>; } else if (props.icon === personalvar.rendering_data.mail) {
               icon = mail;
               label = <a href={personalvar.mailto.mail_with_subject_pl}>
                  {personalvar.label.mail_label}
                  < /a>; if (props.lang === 'en') {
                        label = <a href = {
                           personalvar.mailto.mail_with_subject_en
                        } > {
                           personalvar.label.mail_label
                        } < /a>};
         }
            else if (props.icon === personalvar.rendering_data.in) {
               icon = linkedin;
               label = <a href={personalvar.label.linkedin_link}>{personalvar.label.linkedin_label}</a>

            }
            else if (props.icon === personalvar.rendering_data.book) {
               icon = book;
               if (props.lang === 'en') {
                  label = personalvar.label.book_label;
               } else {
                  label = personalvar.label.book_label_pl;
               }

            }
            else if (props.icon === personalvar.rendering_data.conferences) {
               icon = conferences;
               if (props.lang === 'en') {
                  label = personalvar.label.conferences_label;
               } else {
                  label = personalvar.label.conferences_label_pl;
               }

            }
            else if (props.icon === personalvar.rendering_data.business) {
               icon = business;
               if (props.lang === 'en') {
                  label = personalvar.label.business_label;
               } else {
                  label = personalvar.label.business_label_pl;
               }

            }
            else if (props.icon === personalvar.rendering_data.technical_translations) {
               icon = technical_translations;
               if (props.lang === 'en') {
                  label = personalvar.label.technical_translations_label;
               } else {
                  label = personalvar.label.technical_translations_label_pl;
               }

            }
            else {
               /*icon=filler;
            label="This is filler text";*/
            }

            return (
            <div><img src={icon} alt='icon' className="icons"/>
               <p>
                  {label}
               </p>
            </div>
            )

      };


      export default personal;
