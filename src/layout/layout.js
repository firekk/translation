import React, {Component} from 'react';
import colorless_small_logo from '../assets/images/layout_images/ic_colorless_small_logo.jpg';
import styles from '../assets/stylesheets/main.css';
import Navbar from './navbar';
import Card from '../components/business_card_pattern/card';
import Personal from '../components/personal_pattern/personal';
import ask_for_pricing from '../assets/images/layout_images/ic_ask_for_pricing.svg';
import personalvar from '../components/personal_pattern/personal_variables.json';
import navbar_variables from './navbar_variables.json';
import Footer from './footer';
import footer_data from './footer_data.json';
import Cookie from '../components/cookie/cookie';


class Layout extends Component {

   state = {
      lang: 'pl'
   }

   polishLanguageHandler = () => {
      this.setState({lang: 'pl'})
   }

   englishLanguageHandler = () => {
      this.setState({lang: 'en'})
   }

   render() {

      let header_caption;
      let pricing_link;
      let ask_for_pricing_caption;
      let about_translations;

      if (this.state.lang === 'pl') {
         header_caption = personalvar.header_caption.pl;
         pricing_link = personalvar.mailto.mail_with_subject_pl;
         ask_for_pricing_caption = personalvar.fixed_icon_caption_pl;
         about_translations = navbar_variables.about_translations_pl;
      } else if (this.state.lang === 'en') {
         header_caption = personalvar.header_caption.en;
         pricing_link = personalvar.mailto.mail_with_subject_en;
         ask_for_pricing_caption = personalvar.fixed_icon_caption_en;
         about_translations = navbar_variables.about_translations_en;
      }

      return (<div className="container-fluid">
         <div className="row">
            <Navbar enclick={this.englishLanguageHandler} plclick={this.polishLanguageHandler} lang_contact={this.state.lang} pricing_link_rwd={pricing_link} ask_for_pricing_caption_rwd={ask_for_pricing_caption}/>
         </div>
         <div className="row special_section">
            <div className="col-sm-4 col-md-4 col-lg-4 " align="right">
               <img id="logo" src={colorless_small_logo}/>
            </div>
            <div className="col-sm-4 col-md-4 col-lg-4 " align="right">
               <h2 className="XYZ" id="to_contact">Klaudia F</h2>
               <p lang={this.state.lang}>{about_translations}</p>
            </div>
            <div className="col-sm-4 col-md-4 col-lg-4 " align="left">
               <a href={pricing_link}>
                  <button type="button" className="btn btn-primary btn-sm" id="ask_for_pricing">{ask_for_pricing_caption}</button>
               </a>
            </div>
         </div>
         <div className="row contact" id="to_company">
            <div className="col-sm-4 col-md-4 col-lg-4 ">
               <Personal icon={personalvar.rendering_data.phone_number} lang={this.state.lang}/>
            </div>
            <div className="col-sm-4 col-md-4 col-lg-4 ">
               <Personal icon={personalvar.rendering_data.mail} lang={this.state.lang}/>
            </div>
            <div className="col-sm-4 col-md-4 col-lg-4 ">
               <Personal icon={personalvar.rendering_data. in} lang={this.state.lang}/>
            </div>
         </div>
         <div className="container">
            <div className="row white">
               <div className="col-sm-10 offset-sm-1 col-md-10 offset-sm-1 col-lg-10 offset-lg-1" align="center">
                  <Card lang={this.state.lang} id={"aboutme_carousel_indicators"}/>
               </div>
            </div>
         </div>
         <div className="container icon_style" id="to_translation">
            <div className="row">
               <div className="col-sm-12  col-md-12  col-lg-12" align="right">
                  <h6>{header_caption}</h6>
               </div>
            </div>
            <div className="container icon_style account_style">
               <div className="row icon_bg">
                  <div className="col-sm-6 col-md-6 col-lg-6" align="left">
                     <Personal icon={personalvar.rendering_data.book} lang={this.state.lang}/>
                  </div>
                  <div className="col-sm-6 col-md-6 col-lg-6">
                     <Personal icon={personalvar.rendering_data.conferences} lang={this.state.lang}/>
                  </div>
               </div>
               <div className="row icon_bg">
                  <div className="col-sm-6 col-md-6 col-lg-6" align="left">
                     <Personal icon={personalvar.rendering_data.technical_translations} lang={this.state.lang}/>
                  </div>
                  <div className="col-sm-6 col-md-6 col-lg-6">
                     <Personal icon={personalvar.rendering_data.business} lang={this.state.lang}/>
                  </div>
               </div>
            </div>
            <div className="row white">
               <div className="col-sm-10 offset-sm-1 col-md-10 offset-sm-1 col-lg-10 offset-lg-1" align="center" id="to_opinions">
                  <Card id={"opinions_carousel_indicators"}/>
               </div>
            </div>
         </div>
         <div className="row">
            <Footer lang={this.state.lang} clicked_imprint={this.props.parent_imprint}/>
         </div>
         <div className="row">
            <div>
               <Cookie/>
            </div>
         </div>

      </div>);

   }

}

export default Layout;
