import React, {Component} from 'react';
import footer_data from './footer_data.json';

const footer = (props) => {

   let year = footer_data.footer.year;
   let realization = footer_data.footer.realization;
   let rights;
   let imprint;

   if (props.lang === 'pl') {
      rights = footer_data.footer.rights_pl;
      imprint = footer_data.footer.imprint_pl;

   } else if (props.lang === 'en') {
      rights = footer_data.footer.rights_en;
      imprint = footer_data.footer.imprint_en;
   }

   return (<div className="footer">
      <p>{year}</p>
      <p>{realization}</p>
      <p>{rights}</p>
      <p onClick={props.clicked_imprint}>{imprint}</p>
   </div>);
};

export default footer;
