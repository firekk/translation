import React from 'react';
import pl from '../assets/images/layout_images/ic_flag_pl.svg';
import en from '../assets/images/layout_images/ic_flag_en.svg';
import navbar_variables from './navbar_variables.json';

const navbar = (props) => {

   let contact_caption;
   let company_caption;
   let translation_caption;
   let values_caption;
   let opinions_caption;

   if (props.lang_contact === 'pl') {
      contact_caption = navbar_variables.contact_pl;
      company_caption = navbar_variables.company_pl;
      translation_caption = navbar_variables.translations_pl;
      values_caption = navbar_variables.values_pl;
      opinions_caption = navbar_variables.opinions_pl;

   } else if (props.lang_contact === 'en') {
      contact_caption = navbar_variables.contact_en;
      company_caption = navbar_variables.company_en;
      translation_caption = navbar_variables.translations_en;
      values_caption = navbar_variables.values_en;
      opinions_caption = navbar_variables.opinions_en;

   }
   return (<nav className="navbar fixed-top navbar-expand-lg navbar-light ">

      <button className="navbar-toggler mr-auto  btn-sm" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-label="Toggle navigation">
         <span className="navbar-toggler-icon"></span>
      </button>
      <div>
         <a href={props.pricing_link_rwd}>
            <button type="button" className="btn btn-primary btn-sm" id="ask_for_pricing_rwd">{props.ask_for_pricing_caption_rwd}</button>
         </a>
      </div>
      <div id="flags">
         <a className="navbar-brand navbar-right"><img src={pl} id="pl" width="30" height="30" alt="" onClick={props.plclick}/>
            <img src={en} id="en" width="30" height="30" alt="" onClick={props.enclick}/></a>
      </div>

      <div className="collapse navbar-collapse navbar-light" id="navbarNavAltMarkup">
         <div className="nav navbar-nav ">

            <a className="nav-item nav-link mr-auto" href="#to_company">{company_caption}</a>
            <a className="nav-item nav-link mr-auto" href="#to_translation">{translation_caption}</a>
            <a className="nav-item nav-link mr-auto" href="#to_opinions">{opinions_caption}</a>
            <a className="nav-item nav-link mr-auto" href="#to_contact">{contact_caption}</a>
         </div>
      </div>
   </nav>);
};

export default navbar;
