import React, {Component} from 'react';
import Layout from './layout/layout';
import Imprint from './components/imprint/imprint';
import Footer from './layout/footer';

class App extends Component {

   state = {
      imprint_mood: "mute"
   }
   activateImprintHandler = () => {
      this.setState({imprint_mood: 'activated'});
   }
   render() {

      let layout_component;

      if (this.state.imprint_mood === 'activated') {
         layout_component = <Imprint/>;

      } else {

         layout_component = <Layout parent_imprint={this.activateImprintHandler}/>;
      }

      return (<div>
         {layout_component}
      </div>)
   }
}

export default App;
